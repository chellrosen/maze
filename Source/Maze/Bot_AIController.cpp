// Fill out your copyright notice in the Description page of Project Settings.


#include "Bot_AIController.h"
#include "Bot_TargetPoint.h"
#include "Bot_Char.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "BehaviorTree/BehaviorTree.h"

ABot_AIController::ABot_AIController()
{
	//initialing behavior tree and blackboard component for the controller

	BehaviorTreeComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTree"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard"));

	//key's initialized for the blackboard
	LocationToGoKey = "LocationToGo";
	CurrentTargetPoint = 0; //current target point to go to 

	

}

// overriding the possess function
void ABot_AIController::Possess(APawn* Pawn)
{
	Super::Possess(Pawn);

	//cast the possessed pawn to our AI class
	ABot_Char* Bot_Char = Cast<ABot_Char>(Pawn);

	if (Bot_Char) //make sure cast worked
	{
		if (Bot_Char->BehaviorTree->BlackboardAsset) //make sure we have a blackboard component
		{
			//set the blackboard component for the controller to the - blackboard component on the pawn
			BlackboardComp->InitializeBlackboard(*(Bot_Char->BehaviorTree->BlackboardAsset));
																							  
		}

		//get the array of navigational points - get a reference to all the target points in the level
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABot_TargetPoint::StaticClass(), NavPoints); 

		
		
		//generate random number to decide path
		Seed = FMath::RandRange(1, 2);
		
		if (Seed == 1)
		{
		  // GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, TEXT("Seed 1")); //debug message
		  
		//remove the navigation points that aren't needed for this run
		  NavPoints.RemoveAt(7);
		  NavPoints.RemoveAt(6);
		  NavPoints.RemoveAt(5);
		  NavPoints.RemoveAt(4);
		}
		else if (Seed==2)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, TEXT("Seed 2")); //debug message
	  
		//remove the navigation points that aren't needed for this run
			NavPoints.RemoveAt(0);
			NavPoints.RemoveAt(0);
			NavPoints.RemoveAt(0);
			NavPoints.RemoveAt(0);
		}

		BehaviorTreeComp->StartTree(*Bot_Char->BehaviorTree); //start the behavior tree which will kick off the sequence
	}
}