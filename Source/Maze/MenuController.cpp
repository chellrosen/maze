// Fill out your copyright notice in the Description page of Project Settings.

/*For drawing and creating a HUD and other widgets they need to be created and then added to the view port. For HUD this is often done at the start of the game or level
but I added the success screen that appears on a win condition. The HUD has access to variables that are blueprint viewable so that there is a visual indicator of goals  and player statics

User interfaces are created in the blueprint editor and assigned to different blueprints to load them as a part of begin play or another function*/

#include "MenuController.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"


void AMenuController::BeginPlay()
{
	Super::BeginPlay();
	if (wMainMenu) // Check if the Asset is assigned in the blueprint.
	{
		// Create the widget and store it.
		MyMenu = CreateWidget<UUserWidget>(this, wMainMenu);
		// now you can use the widget directly since you have a reference for it.
		// Extra check to make sure the pointer holds the widget.
		if (MyMenu)
		{
			//let add it to the view port
			MyMenu->AddToViewport();
		}
		//Show the Cursor.
		bShowMouseCursor = true;
	}
}
