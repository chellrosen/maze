// Fill out your copyright notice in the Description page of Project Settings.

#include "Bot_Char.h"
#include "Engine.h"

// Sets default values
ABot_Char::ABot_Char()
{
	PawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Pawn Sensor"));
	if (PawnSensor)
	{
		PawnSensor->SensingInterval = .25f; // 4 times per second
		PawnSensor->SetPeripheralVisionAngle(85.f);
		PawnSensor->bHearNoises = false;

	}
}

// Called when the game starts or when spawned
void ABot_Char::BeginPlay()
{
	Super::BeginPlay();

	//allows the bot to see the player character
	PawnSensor->OnSeePawn.AddDynamic(this, &ABot_Char::OnSeePawn);
}


// Called to bind functionality to input
void ABot_Char::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
}

//when the player is spotted cast to the player character class and call the failure widget
void ABot_Char::OnSeePawn(APawn * OtherPawn)
{
	APlayerMaze* Player = Cast<APlayerMaze>(OtherPawn);

	if (Player)
	{
		Player->DrawFailure();
	}
}

